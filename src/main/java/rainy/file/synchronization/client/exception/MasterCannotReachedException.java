package rainy.file.synchronization.client.exception;

import rainy.file.synchronization.config.RainyRunTime;

public class MasterCannotReachedException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MasterCannotReachedException() {
		super("[" + RainyRunTime.getPropDefine("rainy.master.serverName")
				+ "] can't be reached, please make sure the master is running and the firewall is opened!");
	}
}
